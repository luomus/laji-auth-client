# Laji-auth client

## Laji-auth

Laji-auth mahdollistaa ulkoisten käyttäjien tunnistautumisen lajitietokeskukseen
kotiorganisaationsa käyttäjätunnuksilla. Tämä dokumentti käsittelee laji-authin asiakasohjelmaa, lisätietoja
itse laji-authista löydät sen omasta repositoriosta.

## Client

Tällä kirjastolla voit generoida osoitteita, jonne tunnistautuvat loppukäyttäjät ohjataan ja validoida käyttäjiltä
saatuja tunnistautumistietoja.

### Uudelleenohjaus

```
LajiAuthClient client = new LajiAuthClient("KE.342", "http://laji-auth.esimerkki.fi/");
URI uudelleenohjaus = client.createLoginUrl("/seuraava/").build(); // "http://laji-auth.esimerkki.fi/login?target=KE.342&next=/seuraava/"
```

### Validointi

```
client.validateToken(token);
```

## Asentaminen

```
$ git clone ...
$ cd lajiauth-client
```

Jos projektisi käyttää Mavenia:
```
$ mvn clean install
$ vi /oma/projekti/pom.xml

...
<dependency>
    <groupId>fi.luomus</groupId>
    <artifactId>laji-auth-client</artifactId>
    <version>x.x</version>
</dependency>
...
```

Jos ei:
```
$ mvn clean package
$ cp target/laji-auth-client-x.x.jar /oma/projekti/lib/
```
