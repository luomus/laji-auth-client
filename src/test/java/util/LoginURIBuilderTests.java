package util;

import static org.junit.Assert.assertEquals;

import fi.luomus.lajiauth.model.AuthenticationSource;
import fi.luomus.lajiauth.model.LoginURIBuilder;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.Before;
import org.junit.Test;

public class LoginURIBuilderTests {

	private final URI lajiAuthUri = init();
	private final String targetSystemId = "KE.123";
	private LoginURIBuilder builder;
	
	private URI init() {
		try {
			return new URI("http://lajiauth.com");
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Before
	public void set() {
		builder = new LoginURIBuilder(lajiAuthUri, targetSystemId); 
	}
	
	@Test
	public void test_blank() {
		assertEquals("http://lajiauth.com/login?target=KE.123&redirectMethod=POST", 
				builder.build().toString());
	}

	@Test
	public void test_redirectmethod() {
		assertEquals("http://lajiauth.com/login?target=KE.123&redirectMethod=GET", 
				builder.redirectMethod(HttpMethod.GET).build().toString());
	}
	
	@Test
	public void test_authsource() {
		assertEquals("http://lajiauth.com/auth-sources/FACEBOOK?target=KE.123&redirectMethod=POST", 
				builder.authenticationSource(AuthenticationSource.FACEBOOK).build().toString());
	}
	
	@Test
	public void test_nextparam() {
		assertEquals("http://lajiauth.com/login?target=KE.123&redirectMethod=POST&next=%2FMX.1%3Ffoo%3Dbar", 
				builder.nextParameter("/MX.1?foo=bar").build().toString());
	}

	@Test
	public void test_encodinginnextparam() {
		assertEquals("http://lajiauth.com/login?target=KE.123&redirectMethod=POST&next=%2Fbase%2Fpath%3Fmyparam%3D%3Ffoo%3Dbar", 
				builder.nextParameter("/base/path?myparam=%3Ffoo%3Dbar").build().toString());
	}
	
	@Test
	public void test_path_and_params() {
		MultivaluedMap<String, String > params = new MultivaluedHashMap<>();
		params.add("foo", "bar");
		
		assertEquals("http://lajiauth.com/login?target=KE.123&redirectMethod=POST&next=%2Fbase%2Fpath%3Ffoo%3Dbar", 
				builder.nextParameter("base/path", params).build().toString());
	}
	
	@Test
	public void test_lang() {
		assertEquals("http://lajiauth.com/login?target=KE.123&redirectMethod=POST&language=sv", 
				builder.language("sv").build().toString());
	}
	
	@Test
	public void test_all() {
		assertEquals("http://lajiauth.com/auth-sources/shibboleth/HAKA?target=KE.123&redirectMethod=GET&next=%2Ffoo&language=en", 
				builder.authenticationSource(AuthenticationSource.HAKA).language("en").nextParameter("/foo").redirectMethod(HttpMethod.GET).build().toString());
	}
}
