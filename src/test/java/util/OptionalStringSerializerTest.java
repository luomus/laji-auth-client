package util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;

import fi.luomus.lajiauth.model.UserDetails;

public class OptionalStringSerializerTest {
	
	private final ObjectMapper objectMapper = new ObjectMapper();

	@Test
	public void testPresent() throws IOException {
		UserDetails userDetails = new UserDetails();
		userDetails.setQname(Optional.of("foo"));
		Optional<String> got = objectMapper.readValue(objectMapper.writeValueAsString(userDetails), UserDetails.class).getQname();
		assertEquals("foo", got.get());
	}

	@Test
	public void testAbsent() throws IOException {
		UserDetails userDetails = new UserDetails();
		userDetails.setQname(Optional.<String>absent());
		Optional<String> got = objectMapper.readValue(objectMapper.writeValueAsString(userDetails), UserDetails.class).getQname();
		assertFalse(got.isPresent());
	}

	@Test
	public void testMap() throws IOException {
		UserDetails userDetails = new UserDetails();
		userDetails.setAdditionalUserIds(new HashMap<String, List<String>>());
		userDetails.getAdditionalUserIds().put("foo", new ArrayList<String>());
		userDetails.getAdditionalUserIds().get("foo").add("bar");
		Object o = objectMapper.readValue(objectMapper.writeValueAsString(userDetails), UserDetails.class).getAdditionalUserIds();
		assertEquals("{foo=[bar]}", o.toString());
	}

}