package fi.luomus.lajiauth.service;

import java.net.URI;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.LoginURIBuilder;
import fi.luomus.utils.exceptions.ApiException;

public class LajiAuthClient {

	private final String targetSystemId;
	private final URI lajiAuthUri;
	private WebTarget target;
	private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	/**
	 * @param targetSystemId Triplestore qname of the target system (e.g. KE.123)
	 * @param lajiAuth       The base URI of the laji-auth dev/staging/prod instance (for example https://login.laji.fi)
	 */
	public LajiAuthClient(String targetSystemId, URI lajiAuth) {
		if (targetSystemId == null || targetSystemId.length() == 0) {
			throw new IllegalStateException("System id must be provided");
		}
		if (lajiAuth == null) {
			throw new IllegalStateException("Location of laji-auth must be provided");
		}
		this.targetSystemId = targetSystemId;
		this.lajiAuthUri = lajiAuth;
	}

	/**
	 * Build URI to Laji-Auth with next parameter
	 * @param nextParameter Value of the next parameter will be returned with successful authentication info as it is.
	 * @return Next path parameter builder
	 */
	public LoginURIBuilder createLoginUrl(String nextParameter) {
		return new LoginURIBuilder(lajiAuthUri, targetSystemId, nextParameter);
	}

	/**
	 * Build URI to Laji-Auth with next parameter
	 * @param nextParameter Value of the next parameter will be returned with successful authentication info as it is.
	 * @return Next path parameter builder
	 */
	public LoginURIBuilder createLoginUrl() {
		return new LoginURIBuilder(lajiAuthUri, targetSystemId);
	}

	/**
	 * Get information about a token and validate it is for target system
	 * @param token the token
	 * @return Autentication info
	 * @throws ApiException
	 */
	public AuthenticationEvent getAndValidateAuthenticationInfo(String token) throws ApiException {
		Response response = target().path("token/"+token).request().get();
		raiseForNonSuccess(response);

		String authenticationInfoJson = response.readEntity(String.class);
		AuthenticationEvent authenticationInfo;
		try {
			authenticationInfo = objectMapper.readValue(authenticationInfoJson, AuthenticationEvent.class);
		} catch (Exception e) {
			throw new ValidationException("Authentication information deserialisation failed");
		}

		raiseForInvalidTarget(authenticationInfo);

		return authenticationInfo;
	}

	/**
	 * Invalidate a token
	 * @param token
	 * @throws ApiException
	 */
	public void invalidateToken(String token) throws ApiException {
		Response response = target().path("token/"+token).request().delete();
		raiseForNonSuccess(response);
	}

	private void raiseForInvalidTarget(AuthenticationEvent authenticationInfo) {
		if (!targetSystemId.equals(authenticationInfo.getTarget())) {
			throw new ValidationException("Authentication token target system id does not match the specified target system id. Expected" + targetSystemId + " got " + authenticationInfo.getTarget());
		}
	}

	public static class ValidationException extends RuntimeException {
		private static final long serialVersionUID = -1354941610455656346L;
		public ValidationException(String cause) {
			super(cause);
		}
	}

	private WebTarget target() {
		if (this.target == null) {
			// Prevent duplicate target creations
			synchronized (this) {
				if (this.target == null) {
					Client client = ClientBuilder.newBuilder()
							.build();

					this.target = client.target(lajiAuthUri);
				}
			}
		}
		return target;
	}

	private void raiseForNonSuccess(Response response) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			throw ApiException.builder().source("LAJI_AUTH")
			.code(Integer.toString(response.getStatus()))
			.details(getCause(response))
			.build();
		}
	}

	private String getCause(Response response) {
		String cause = null;
		try {
			cause = response.readEntity(String.class);
		} catch (ProcessingException e) {}
		return cause;
	}

}
