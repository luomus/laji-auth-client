package fi.luomus.lajiauth.model;

import static fi.luomus.lajiauth.model.Constants.AUTH_SOURCES;

import java.net.URI;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

public class LoginURIBuilder {

	private final URI lajiAuthUri; 
	private final String targetSystemId;
	private final String nextParameter;
	private final AuthenticationSource authenticationSource;
	private final String language;
	private final String redirectMethod;
	private final boolean offerPermanent;

	public LoginURIBuilder(URI lajiAuthUri, String targetSystemId) {
		this(lajiAuthUri, targetSystemId, null, null, null, HttpMethod.POST, false);
	}

	public LoginURIBuilder(URI lajiAuthUri, String targetSystemId, String nextParameter) {
		this(lajiAuthUri, targetSystemId, nextParameter, null, null, HttpMethod.POST, false);
	}

	private LoginURIBuilder(URI lajiAuthUri, String targetSystemId, String nextParameter, AuthenticationSource authenticationSource, String language, String redirectMethod, boolean offerPermanent) {
		this.lajiAuthUri = lajiAuthUri;
		this.targetSystemId = targetSystemId;
		this.nextParameter = nextParameter;
		this.authenticationSource = authenticationSource;
		this.language = language;
		this.redirectMethod = redirectMethod;
		this.offerPermanent = offerPermanent;
	}

	/**
	 * Set value of next parameter that will be returned with successful authentication info as it is
	 * @param nextParameter
	 * @return
	 */
	public LoginURIBuilder nextParameter(String nextParameter) {
		return new LoginURIBuilder(lajiAuthUri, targetSystemId, nextParameter, authenticationSource, language, redirectMethod, offerPermanent);
	}

	/**
	 * Set value of next parameter by providing (relative) path and query params
	 * @param nextPath without '/' in front
	 * @param query query parameters
	 * @return
	 */
	public LoginURIBuilder nextParameter(String nextPath, MultivaluedMap<String, String> query) {
		return new LoginURIBuilder(lajiAuthUri, targetSystemId, parseNext(nextPath, query), authenticationSource, language, redirectMethod, offerPermanent);
	}

	private String parseNext(String nextPath, MultivaluedMap<String, String> query) {
		UriBuilder nextParam = UriBuilder.fromPath("/"+nextPath);
		for (String key : query.keySet()) {
			for (String value : query.get(key)) {
				nextParam = nextParam.queryParam(key, value);
			}
		}
		return nextParam.build().toString();
	}

	/**
	 * Make the user re-direct directly to the specified authentication source
	 *
	 * @param authenticationSource The authentication source page the user should be redirected to
	 * @return The builder
	 */
	public LoginURIBuilder authenticationSource(AuthenticationSource authenticationSource) {
		return new LoginURIBuilder(lajiAuthUri, targetSystemId, nextParameter, authenticationSource, language, redirectMethod, offerPermanent);
	}

	/**
	 * Force the specified language for the user
	 *
	 * @param language The language code (lowercase ISO code e.g. "fi")
	 * @return The builder
	 */
	public LoginURIBuilder language(String language) {
		return new LoginURIBuilder(lajiAuthUri, targetSystemId, nextParameter, authenticationSource, language, redirectMethod, offerPermanent);
	}

	/**
	 * Define HTTP method (GET or POST) for redirect
	 * @param redirectMethod
	 * @return
	 */
	public LoginURIBuilder redirectMethod(String redirectMethod) {
		return new LoginURIBuilder(lajiAuthUri, targetSystemId, nextParameter, authenticationSource, language, redirectMethod, offerPermanent);
	}

	/**
	 * Will create a non-expiring token
	 * @param offerPermanent
	 * @return
	 */
	public LoginURIBuilder offerPermanent(boolean offerPermanent) {
		return new LoginURIBuilder(lajiAuthUri, targetSystemId, nextParameter, authenticationSource, language, redirectMethod, offerPermanent);
	}

	/**
	 * Build Laji-Auth login URI using specified parameters
	 *
	 * @return The redirect URI that the user should be redirected to
	 */
	public URI build() {
		UriBuilder uriBuilder = UriBuilder.fromUri(this.lajiAuthUri)
				.queryParam(Constants.TARGET_SYSTEM_PARAMETER, this.targetSystemId)
				.queryParam(Constants.REDIRECT_METHOD, this.redirectMethod);

		if (this.nextParameter != null) {
			uriBuilder = uriBuilder.queryParam(Constants.NEXT_PARAMETER, this.nextParameter);
		}
		if (this.language != null) {
			uriBuilder = uriBuilder.queryParam(Constants.LANGUAGE_PARAMETER, this.language);
		}

		if (this.authenticationSource != null) {
			uriBuilder = uriBuilder.path(AUTH_SOURCES);
			if (this.authenticationSource == AuthenticationSource.VIRTU || this.authenticationSource == AuthenticationSource.HAKA) {
				uriBuilder = uriBuilder.path("shibboleth");
			}
			uriBuilder = uriBuilder.path(this.authenticationSource.toString());
		} else {
			uriBuilder = uriBuilder.path("login");
		}

		if (this.offerPermanent) {
			uriBuilder.queryParam(Constants.OFFER_PERMANENT, this.offerPermanent);
		}

		return uriBuilder.build();
	}

}
