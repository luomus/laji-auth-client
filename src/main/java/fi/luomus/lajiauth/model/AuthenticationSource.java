package fi.luomus.lajiauth.model;

public enum AuthenticationSource {
	HAKA,
	VIRTU,
	FACEBOOK,
	GOOGLE,
	LOCAL,
	INATURALIST,
	OMARIISTA,
	APPLE
}
