package fi.luomus.lajiauth.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Optional;

import util.OptionalIntegerDeserializer;
import util.OptionalIntegerSerializer;
import util.OptionalStringDeserializer;
import util.OptionalStringSerializer;

public class UserDetails implements Serializable {

	private static final long serialVersionUID = -8008229940783317094L;

	private String authSourceId;

	@JsonDeserialize(using = OptionalStringDeserializer.class)
	@JsonSerialize(using = OptionalStringSerializer.class)
	private Optional<String> qname = Optional.absent();

	@JsonDeserialize(using = OptionalIntegerDeserializer.class)
	@JsonSerialize(using = OptionalIntegerSerializer.class)
	private Optional<Integer> yearOfBirth = Optional.absent();

	@JsonDeserialize(using = OptionalStringDeserializer.class)
	@JsonSerialize(using = OptionalStringSerializer.class)
	private Optional<String> firstJoined = Optional.absent();

	@JsonDeserialize(using = OptionalStringDeserializer.class)
	@JsonSerialize(using = OptionalStringSerializer.class)
	private Optional<String> preferredName = Optional.absent();

	@JsonDeserialize(using = OptionalStringDeserializer.class)
	@JsonSerialize(using = OptionalStringSerializer.class)
	private Optional<String> inheritedName = Optional.absent();

	private String name;
	private String email;

	@JsonDeserialize(using = OptionalStringDeserializer.class)
	@JsonSerialize(using = OptionalStringSerializer.class)
	private Optional<String> defaultLanguage;

	@JsonDeserialize(using = OptionalStringDeserializer.class)
	@JsonSerialize(using = OptionalStringSerializer.class)
	private Optional<String> group;

	private Set<String> roles = new HashSet<>();
	private Set<String> organisations = new HashSet<>();
	private Set<String> previousEmailAddresses = new HashSet<>();
	private Map<String, List<String>> additionalUserIds = new HashMap<>();

	@JsonDeserialize(using = OptionalStringDeserializer.class)
	@JsonSerialize(using = OptionalStringSerializer.class)
	private Optional<String> address;

	/**
	 * @return User's locally unique id in the source authentication system
	 */
	public String getAuthSourceId() {
		return authSourceId;
	}

	public void setAuthSourceId(String authSourceId) {
		this.authSourceId = authSourceId;
	}

	/**
	 * @return User's full name
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return User's globally unique id in the ontology database, if such exists
	 */
	public Optional<String> getQname() {
		return qname;
	}

	public void setQname(Optional<String> qname) {
		this.qname = qname;
	}

	/**
	 * @return User's email address
	 */
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email == null ? null : email.toLowerCase().trim();
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	public Optional<Integer> getYearOfBirth() {
		return yearOfBirth;
	}

	public void setYearOfBirth(Optional<Integer> yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}

	public Optional<String> getFirstJoined() {
		return firstJoined;
	}

	public void setFirstJoined(Optional<String> firstJoined) {
		this.firstJoined = firstJoined;
	}

	public Optional<String> getPreferredName() {
		return preferredName;
	}

	public void setPreferredName(Optional<String> preferredName) {
		this.preferredName = preferredName;
	}

	public Optional<String> getInheritedName() {
		return inheritedName;
	}

	public void setInheritedName(Optional<String> inheritedName) {
		this.inheritedName = inheritedName;
	}

	public Set<String> getOrganisations() {
		return organisations;
	}

	public void setOrganisations(Set<String> organizations) {
		this.organisations = organizations;
	}

	public Set<String> getPreviousEmailAddresses() {
		return previousEmailAddresses;
	}

	public void setPreviousEmailAddresses(Set<String> previousEmailAddresses) {
		this.previousEmailAddresses = previousEmailAddresses;
	}

	public Optional<String> getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(Optional<String> defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public Optional<String> getGroup() {
		return group;
	}

	public void setGroup(Optional<String> group) {
		this.group = group;
	}

	public Map<String, List<String>> getAdditionalUserIds() {
		return additionalUserIds;
	}

	public void setAdditionalUserIds(Map<String, List<String>> additionalUserIds) {
		this.additionalUserIds = additionalUserIds;
	}

	public Optional<String> getAddress() {
		return address;
	}

	public void setAddress(Optional<String> address) {
		this.address = address;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UserDetails that = (UserDetails) o;
		if (authSourceId != null ? !authSourceId.equals(that.authSourceId) : that.authSourceId != null) return false;
		if (qname != null ? !qname.equals(that.qname) : that.qname != null) return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = authSourceId != null ? authSourceId.hashCode() : 0;
		result = 31 * result + (qname != null ? qname.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "UserDetails [authSourceId=" + authSourceId + ", qname=" + qname + ", yearOfBirth=" + yearOfBirth + ", firstJoined=" + firstJoined + ", preferredName="
				+ preferredName + ", inheritedName=" + inheritedName + ", name=" + name + ", email=" + email + ", defaultLanguage=" + defaultLanguage + ", group=" + group
				+ ", roles=" + roles + ", organisations=" + organisations + ", previousEmailAddresses=" + previousEmailAddresses + ", additionalUserIds=" + additionalUserIds
				+ ", address=" + address + "]";
	}

}

