package fi.luomus.lajiauth.model;

public class Constants {

	public final static String TARGET_SYSTEM_PARAMETER = "target";
	public final static String NEXT_PARAMETER = "next";
	public final static String LANGUAGE_PARAMETER = "language";
	public final static String AUTH_SOURCES = "auth-sources";
	public final static String REDIRECT_METHOD = "redirectMethod";
	public final static String OFFER_PERMANENT = "offerPermanent";

}
