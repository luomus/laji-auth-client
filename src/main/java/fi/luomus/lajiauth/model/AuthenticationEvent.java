package fi.luomus.lajiauth.model;

import java.io.Serializable;

import javax.ws.rs.HttpMethod;

public class AuthenticationEvent implements Serializable {

	private static final long serialVersionUID = 8792446542946678646L;
	private UserDetails user;
	private AuthenticationSource source;
	private String target;
	private String next;
	private String redirectMethod = HttpMethod.POST;
	private boolean permanent = false;

	public UserDetails getUser() {
		return user;
	}

	public void setUser(UserDetails user) {
		this.user = user;
	}

	public AuthenticationSource getSource() {
		return source;
	}

	public void setSource(AuthenticationSource source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getRedirectMethod() {
		return redirectMethod;
	}

	public void setRedirectMethod(String redirectMethod) {
		if (redirectMethod == null || redirectMethod.isEmpty()) {
			this.redirectMethod = HttpMethod.POST;
		} else {
			if (!redirectMethod.equals(HttpMethod.POST) && !redirectMethod.equals(HttpMethod.GET)) {
				throw new IllegalArgumentException("Method must be one of " + HttpMethod.POST + ", " + HttpMethod.GET);
			}
			this.redirectMethod = redirectMethod;
		}
	}

	public boolean isPermanent() {
		return permanent;
	}

	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((next == null) ? 0 : next.hashCode());
		result = prime * result + (permanent ? 1231 : 1237);
		result = prime * result + ((redirectMethod == null) ? 0 : redirectMethod.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthenticationEvent other = (AuthenticationEvent) obj;
		if (next == null) {
			if (other.next != null)
				return false;
		} else if (!next.equals(other.next))
			return false;
		if (permanent != other.permanent)
			return false;
		if (redirectMethod == null) {
			if (other.redirectMethod != null)
				return false;
		} else if (!redirectMethod.equals(other.redirectMethod))
			return false;
		if (source != other.source)
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthenticationEvent [user=" + user + ", source=" + source + ", target=" + target + ", next=" + next + ", redirectMethod=" + redirectMethod + ", permanent="
				+ permanent + "]";
	}

}
