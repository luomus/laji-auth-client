package util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Optional;

public class OptionalIntegerDeserializer extends JsonDeserializer<Optional<Integer>> {
	
    @Override
    public Optional<Integer> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return Optional.of(jp.getValueAsInt());
    }

    @Override
    public Optional<Integer> getNullValue() {
        return Optional.absent();
    }
    
}
