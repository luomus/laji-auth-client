package util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.common.base.Optional;

public class OptionalIntegerSerializer extends JsonSerializer<Optional<Integer>> {
	
    @Override
    public void serialize(Optional<Integer> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
    	if (value.isPresent()) {
    		jgen.writeNumber(value.get());
    	} else {
    		jgen.writeNull();
    	}
    }
    
}
