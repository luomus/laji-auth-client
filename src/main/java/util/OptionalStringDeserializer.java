package util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Optional;

public class OptionalStringDeserializer extends JsonDeserializer<Optional<String>> {
	
    @Override
    public Optional<String> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return Optional.fromNullable(jp.getValueAsString());
    }

    @Override
    public Optional<String> getNullValue() {
        return Optional.absent();
    }
    
}
